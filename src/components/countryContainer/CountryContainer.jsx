import React from "react";
import "./style.css";
import { useNavigate } from "react-router-dom";

function CountryContainer(props) {
  const { country } = props;
  const navigate = useNavigate();

  return (
    <div
      className="container"
      onClick={() => navigate(`country/${country.cca2}`)}
    >
      <img src={country.flags.png} alt={country.flags.alt} className="flags" />
      <div className="details">
        <div className="countryName">{country.name.common}</div>
        <div className="population">
          <span className="detailText">Population</span> :{country.population}
        </div>
        <div className="region">
          <span className="detailText">Region</span> : {country.region}
        </div>
        <div className="capital">
          <span className="detailText">Capital</span> : {country.capital}
        </div>
      </div>
    </div>
  );
}

export default CountryContainer;
