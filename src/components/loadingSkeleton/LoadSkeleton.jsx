import Skeleton from "react-loading-skeleton";
import "./style.css";

let LoadSkeleton = ({ loop }) => {
  return (
    <>
      {Array(loop)
        .fill(0)
        .map((_, i) => {
          return (
            <div key={i} className="skeleton">
              <Skeleton height={"50%"} style={{ marginBottom: "0.5rem" }} />
              <div className="content-skeleton">
                <Skeleton
                  count={4}
                  height={"1rem"}
                  width={"100%"}
                  style={{ marginBottom: "0.5rem" }}
                />
              </div>
            </div>
          );
        })}
    </>
  );
};
export default LoadSkeleton;
