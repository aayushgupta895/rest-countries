import React from "react";
import "./style.css";
import { useMyContext } from "../../MyContext";

function Filter(props) {
  const { darkMode } = useMyContext();
  const {
    setSearch,
    regionFilter,
    setRegionFilter,
    subRegionsAvailable,
    setSubRegion,
    subRegion,
    setSortBy,
    sortBy,
  } = props;
  return (
    <>
      <div className={`selection ${darkMode ? "sel-dark" : ""}`}>
        <div className="countryInput">
          <i className="fa-solid fa-magnifying-glass"></i>
          <input
            type="text"
            placeholder="Search for a Country..."
            onChange={(e) => {
              setSearch(e.target.value.trim());
            }}
          />
        </div>
        <div className="select-filter">
          <select
            id="regions"
            name="continent"
            value={regionFilter}
            onChange={(e) => {
              setRegionFilter(e.target.value);
              setSubRegion("");
            }}
          >
            <option value="">Filter by Region</option>
            <option value="Asia">asia</option>
            <option value="Africa">africa</option>
            <option value="Americas">america</option>
            <option value="Oceania">Australia</option>
            <option value="Europe">Europe</option>
          </select>
        </div>
        <div className="select-filter">
          <select
            id="subregions"
            name="subregion"
            value={subRegion}
            onChange={(e) => setSubRegion(e.target.value.trim())}
          >
            <option value="">Filter by subregion</option>
            {subRegionsAvailable[regionFilter] &&
              subRegionsAvailable[regionFilter].map((subrgn) => {
                return (
                  <option key={subrgn} value={`${subrgn}`}>
                    {subrgn}
                  </option>
                );
              })}
          </select>
        </div>
        <div className="select-filter">
          <select
            id="sort"
            name="sort"
            value={sortBy}
            onChange={(e) => setSortBy(e.target.value.trim())}
          >
            <option value="">sort by</option>
            <option value="population-desc">population (high to low)</option>
            <option value="population-asc">population (low to high)</option>
            <option value="area-desc">Area (high to low)</option>
            <option value="area-asc">Area (low to high)</option>
          </select>
        </div>
      </div>
    </>
  );
}

export default Filter;
