import React from "react";
import "./style.css";
import { useMyContext } from "../../MyContext";

function Navbar() {
  function enableDarkMode(){
    setDarkMode(!darkMode);
  }
  const {darkMode, setDarkMode } = useMyContext();
  return (
    <nav className={`${darkMode ? "nav-dark" : ""}`}>
      <h1>Where in the world?</h1>
      <div className="darkToggler">
        <i className="fa-solid fa-moon"></i>
        <i className="fa-regular fa-moon"></i>
        <p onClick={()=>enableDarkMode()}>{darkMode ? `Light Mode` : `Dark Mode`}</p>
      </div>
    </nav>
  );
}

export default Navbar;
